exports.NODE_ENV = process.env.NODE_ENV || "development";
exports.APP_PORT = process.env.APP_PORT || 8080;
exports.APP_NAME = process.env.APP_NAME || "Express JS";
